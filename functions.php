<?php

/**
 * Peak Fine Art Services Theme
 *
 * @package Boldface\Peak
 *
 * @link    https://gitlab.com/boldface/peak/
 * @version 1.0.2
 * @author  Boldface Design Group
 * @link    http://www.boldfacedesign.com/
 * @license GNU General Public License v3 or later
 * @link    https://www.gnu.org/licenses/gpl-3.0.html
 */

declare( strict_types = 1 );
namespace Boldface\Peak;

/**
 * Add updater controller to the admin_init hook
 *
 * @since 1.0
 */
function init() {
  require __DIR__ . '/src/updater.php';
  $updater = new updater();
  $updater->init();
}
\add_action( 'init', __NAMESPACE__ . '\init' );

/**
 * Filter the modules list.
 *
 * @since 1.0
 *
 * @param array $list List of modules.
 *
 * @return array The new list of modules.
 */
function controllersList( array $list ) : array {
  $list[] = 'featuredImage';
  $list[] = 'gallery';
  $list[] = 'images';
  $list[] = 'openGraph';
  $list[] = 'googleFonts';
  $list[] = 'securityHeaders';
  return $list;
}
\add_filter( 'Boldface\Bootstrap\Controllers\modules', __NAMESPACE__ . '\controllersList' );

/**
 * Enqueue the styles and scripts in the footer
 *
 * @since 1.0
 */
function footer() {
  \wp_enqueue_script( 'js-cookie', \get_stylesheet_directory_uri() . '/assets/js/js.cookie.js' );
  \wp_enqueue_script( 'peak', \get_stylesheet_directory_uri() . '/assets/js/peak.js', [ 'jquery' ] );
  \wp_enqueue_style( 'peak', \get_stylesheet_directory_uri() . '/assets/css/peak.css', [ 'bootstrap' ] );

  if( \is_front_page() ) {
    \wp_enqueue_script( 'peak-home', \get_stylesheet_directory_uri() . '/assets/js/home.js', [ 'jquery', 'js-cookie' ] );
    \wp_enqueue_style( 'peak-home', \get_stylesheet_directory_uri() . '/assets/css/home.css', [ 'peak' ] );
  }
}
\add_action( 'wp_footer', __NAMESPACE__ . '\footer' );

/**
 * Wrap H2 headings with div
 *
 * @access public
 * @since 1.0
 *
 * @param string $content The content
 *
 * @return string The content with the H2 headings wrapped with divs
 */
function the_content( string $content ) : string {
  if( false ===  $pos = mb_strpos( $content, '<h2' ) ) return $content;
  return mb_substr( $content, 0, $pos ) . array_reduce( explode( '<h2', mb_substr( $content, $pos ) ), function( $carry, $item ) {
    if( empty( $item ) ) return $carry;
    return $carry . sprintf(
      '<div class="%1$s">%2$s</div>',
      \apply_filters( 'Boldface\Bootstrap\Model\loop\h2\class', 'wrapper' ),
      \apply_filters( 'Boldface\Bootstrap\Model\loop\h2\item', "<h2$item" )
    );
  });
}
\add_filter( 'the_content', __NAMESPACE__ . '\the_content' );

/**
 * Return the filtered h2 wrapper class by adding `panel` on the front page
 *
 * @since 1.0
 *
 * @param string $class The unfiltered class
 *
 * @return string The filtered h2 wrapper class
 */
function front_page_h2_wrapper_class( string $class ) : string {
  return \is_front_page() ? $class . ' panel' : $class;
}
\add_filter( 'Boldface\Bootstrap\Model\loop\h2\class', __NAMESPACE__ . '\front_page_h2_wrapper_class' );

/**
 * Return the content class, modified only on the front page.
 *
 * @since 1.0
 *
 * @param string $class The original content class.
 *
 * @return string The possibly modified content class.
 */
function contentClass( string $class ) : string {
  return \is_front_page() ? 'd-flex flex-wrap flex-lg-nowrap align-content-stretch' : $class;
}
\add_filter( 'Boldface\Bootstrap\Views\entry\content\class', __NAMESPACE__ . '\contentClass');

/**
 * Return the loop class as `container-fluid`
 *
 * @since 1.0
 *
 * @param string $class Unused
 *
 * @return string `container-fluid`
 */
function loopClass( string $class ) : string {
  return ( \is_front_page() || \has_post_thumbnail() ) ? 'container-fluid' : 'container';
}
\add_filter( 'Boldface\Bootstrap\Views\loop\class', __NAMESPACE__ . '\loopClass' );

/**
 * Return the modified h2 item.
 *
 * Locate and remove the first image and h2 tag and place them into a hidden
 * element that will display on hover and small devices.
 *
 * @since 1.0
 *
 * @param string $item The unmodified h2 item
 *
 * @return string The modified h2 item
 */
function loopH2Item( string $item ) : string {
  if( ! \is_front_page() ) return $item;

  // Remove the first img tag
  $imgPos = strpos( $item, '<img' );
  if( false === $imgPos ) return $item;
  $imgEnd = strpos( $item, '>', $imgPos );
  if( false === $imgEnd ) return $item;
  $img = substr( $item, $imgPos, $imgEnd - $imgPos + 1 );
  $item = str_replace( $img, '', $item );

  // Remove the first h2 tag
  $h2Pos = strpos( $item, '<h2' );
  if( false === $h2Pos ) return $item;
  $h2End = strpos( $item, '</h2>', $h2Pos );
  if( false === $h2End ) return $item;
  $h2 = substr( $item, $h2Pos, $h2End - $h2Pos + 5 );
  $item = str_replace( $h2, '', $item );

  // Trim whitespace and remove all other tags
  $item = trim( strip_tags( $item, '<a>' ) );
  return sprintf( '%1$s<p>%2$s<span class="hidden">%3$s</span></p>', $h2, $img, $item );
}
\add_filter( 'Boldface\Bootstrap\Model\loop\h2\item', __NAMESPACE__ . '\loopH2Item' );

/**
 * Return the footer text
 *
 * @since 1.0
 *
 * @param string $text Unused
 *
 * @return string The footer text
 */
function footerText( string $text ) : string {
  return '<p>Peak Fine Art Services, Inc. | P.O. Box 7972 | Aspen, CO 81612 | PH: 970-922-1913 | Fax: 970-922-1914 | email: <a href="mailto:customerservice@peakfas.com">customerservice@peakfas.com</a></p>';
}
\add_filter( 'Boldface\Bootstrap\Views\footer\text', __NAMESPACE__ . '\footerText' );

/**
 * Return the footer class. Remove bg-light and bg-dark. Add bg-peak-green.
 *
 * @since 1.0
 *
 * @param string $class The old footer class
 *
 * @return string The new footer class
 */
function footerClass( string $class ) : string {
  $class = str_replace( [ 'bg-light', 'bg-dark' ], '', $class );
  return 'bg-peak-green ' . $class;
}
\add_filter( 'Boldface\Bootstrap\Views\footer\class', __NAMESPACE__ . '\footerClass' );

/**
 * Return the footer wrapper class.
 *
 * @since 1.0
 *
 * @param string $class The old footer wrapper class. Unused.
 *
 * @return string The new footer wrapper class.
 */
function footerWrapperClass( string $class ) : string {
  return 'text-light container';
}
\add_filter( 'Boldface\Bootstrap\Views\footer\wrapper\class', __NAMESPACE__ . '\footerWrapperClass' );

/**
 * Return the entry header class. Add `peak-green`.
 *
 * @since 1.0
 *
 * @param string $class The old entry header class.
 *
 * @return string The new entry header class.
 */
function entryHeaderClass( string $class ) : string {
  return $class . ' peak-green';
}
\add_filter( 'Boldface\Bootstrap\Views\entry\header\class', __NAMESPACE__ . '\entryHeaderClass' );

/**
 * Return the navigation class. Replace the background-color with Peak grey
 *
 * @since 1.0
 *
 * @param string $class The old navigation class.
 *
 * @return string The new navigation class.
 */
function navigationClass( string $class ) : string {
  $class = str_replace( [ 'bg-dark', 'bg-light' ], 'bg-peak-grey', $class );
  return $class;
}
\add_filter( 'Boldface\Bootstrap\Views\navigation\class', __NAMESPACE__ . '\navigationClass' );

/**
 * Return whether to do the Bootstrap autoRow procedure. True everywhere except the front page.
 *
 * @since 1.0
 *
 * @param bool $bool Unused.
 *
 * @return bool Whether to do the Bootstrap autoRow
 */
function autoRow( bool $bool ) : bool {
  return ! \is_front_page();
}
\add_filter( 'Boldface\Bootstrap\Models\entry\autoRow', __NAMESPACE__ . '\autoRow' );

/**
 * Return the SVG logo as the brand.
 *
 * @since 1.0
 *
 * @param string $brand The original brand.
 *
 * @return string The SVG logo if it exists, otherwise $brand.
 */
function brand( string $brand ) : string {
  if( ! file_exists( \get_stylesheet_directory() . '/assets/images/logo.png' ) )
    return $brand;

  return sprintf(
    '<img width="50" height="50" src="%1$s" alt="Bootstrap Brand Logo" class="img img-fluid img-brand"/>',
    \esc_url( \get_stylesheet_directory_uri() . '/assets/images/logo.svg' )
  );
}
\add_filter( 'Boldface\Bootstrap\Models\navigation\brand', __NAMESPACE__ . '\brand' );

/**
 * Return the new Google Fonts.
 *
 * @since 1.0
 *
 * @param array $fonts The old Google Fonts.
 *
 * @return array The new Google Fonts.
 */
function googleFonts( array $fonts ) : array {
  return [ 'Lato', 'Encode+Sans' ];
}
\add_filter( 'Boldface\Bootstrap\Models\googleFonts', __NAMESPACE__ . '\googleFonts' );

/**
 * Add filter to bypass the Bootstrap REST API.
 *
 * @since 1.0
 */
\add_filter( 'Boldface\Bootstrap\REST', '__return_false' );
