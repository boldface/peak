(function($) {
  $(document).ready(function() {
    var question = 'Click here to enter';

    $(".panel .img").each(function(i,elem){
      var img = $(elem);

      var div = $(this).closest('.panel').addClass('background-image').css({
        "background-image": "url("+img.attr("src")+")",
      });
      img.replaceWith('');
    });

    if( 'true' === Cookies.get( 'PeakQuestion' ) ) {
      return;
    }

    $('.home #navbarCollapse').addClass('display-none');
    $('.home nav').css('height','calc( 100vh - 50px )');
    $('.navbar-brand').css('width','100%');
    $('.navbar-brand .img').css('max-height','initial');
    $('.navbar-brand-name').css('display','none');
    $('.navbar-toggler').css('display','none');

    $(".home nav").append('<div class="question peak-green"><p>'+question+'</p></div>');

    $(".question").click(function() {
      $('nav').css('transition','height 1.2s ease-in-out');
      $(this).css('display','none');
      $('.home nav').css('height','50px').css('width','auto');
      Cookies.set( 'PeakQuestion', 'true', 1 );
    });

    $(".home .navbar-brand").css('cursor','default').click(function(e){
      e.preventDefault();
    });
  });
})(jQuery);
