
(function($){

  $(document).ready(function() {
    $( '.navbar-brand' ).append('<div class="navbar-brand-name" style="display:inline-block"><span class="text-white">Peak Fine Art Services, Inc.</span></div>');

    var postThumbnail = $( '.post-thumbnail img' );
    postThumbnail.css('display','none');
    var src = postThumbnail.attr( 'src' );
    $( '.post-thumbnail' ).css( 'background-image', 'URL(' + src + ')');
  });

})(jQuery);
