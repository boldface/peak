<?php

/**
 * @package Boldface/Peak
 */
declare( strict_types = 1 );
namespace Boldface\Peak;

/**
 * Class for updating the theme
 *
 * @since 1.0
 */
class updater extends \Boldface\Bootstrap\updater {
  protected $theme = 'peak';
  protected $repository = 'boldface/peak';
}
